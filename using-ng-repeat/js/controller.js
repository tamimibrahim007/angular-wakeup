app.controller('MainController',['$scope',function($scope){
	$scope.hello = 'Hello world';
	$scope.items = [
		{
			"name" : "A Great product",
			"desc"	: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique culpa libero quibusdam ea maxime inventore voluptatibus sint sequi amet iste officiis quam, accusantium labore facere repellendus tenetur fugit quisquam assumenda?"
		},
		{
			"name" : "Another Great product",
			"desc" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint quo, est. Et aspernatur, incidunt. Obcaecati, cumque iste ad, aliquam, voluptates quaerat blanditiis sequi eos quam, quos perspiciatis? Assumenda vero, odio."
		},
		{
			"name" : "Not a good product",
			"desc" : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti eius ipsum laudantium harum distinctio laborum deserunt, consequatur ratione optio. Reprehenderit sequi eius nobis natus numquam provident aperiam explicabo eveniet at."
		}

	];
}]);